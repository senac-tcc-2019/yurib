## Unreleased - 10/12
- Added
    - Logs for clonned repositories
- Changed
    - Automatic open build logs when re-build
    - Add @COPY on RUN when create a new repository - not loop over repositories for make dockerfile anymore. 
- Fixes
    - Fix repository not clonning

### Unreleased - 01/12
- Added
    - Remove Container / Data Volumes button
    - Remove App (Config)
    - Alot of tooltips
    - Gilab link/icon on Header
- Changed
    - Now every container is in the same EXTERNAL network called 'dockerizy'
    - Logs design (using a lib)
    - Start/Stop App now only runs docker-compose start/stop, not up anymore.
    - If Start fails, use docker-compose up in this case.


### Unreleased - 29/11
- Added
    - CODE-SERVER - Open repository on VSCode (Browser)
- Changed
    - Reverse proxy


### Unreleased - 27/11
- Added
    - Dynamic local domains with hosts
    - User can see the service/port local domain on Dashboard
    - Reverse Proxy
    - Edit Up CMD
    - Edit Ports
    - Remove App (backend only)
- Changed
    - Removed 'src' folder from Apps folder
    - Changed 'volumes' to 'repositories' folder name


### Unreleased - 25/11
- Added
    - This CHANGELOG.md
    - Build commands (RUN) modal/crud
    - Get App CMD Route
    - Get App Build Logs Route
    - Now user can see Build Logs on Logs Modal
    - Now user can see container CMD on Dashboard
- Changed
    - Only builds image when all repositories finish cloning
    - Repositories are now Copied to inside container
    - Cmds(Commands) to Runs(Build)
    - Refresh App nows save build logs on memory(for now) (used by Get App Build Logs Route)
    - Changed all modals but Logs style to a lighter/white version
    - Show Logs Loading changed position to inside "Container" tab on modal
    - Removed non used fields of App Model (volumes, runs)
    - Volumes removed (needs to think about another method for sync files and not override at same tim )
- Fixed
    - Run CMDs Order
    - Stops Repeating WorkingDir at Dockerfile