import Vue from 'vue'
import Router from 'vue-router'
import Apps from '@/screens/Apps'
import NewApp from '@/screens/NewApp'
import Dashboard from '@/screens/Dashboard'

Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/',
      name: 'Apps',
      component: Apps
    },
    {
      path: '/new-app',
      name: 'NewApp',
      component: NewApp
    },
    {
      path: '/:appname/dashboard',
      name: 'Dashboard',
      component: Dashboard
    }
  ]
})
