import Vue from 'vue'
import App from './App'
import router from './router'
import tooltip from './components/Directives/tooltip'
import fixedTooltip from './components/Directives/fixedTooltip'
import './assets/css/reset.css'

Vue.config.productionTip = false

/* eslint-disable no-new */
new Vue({
  el: '#app',
  directives: {tooltip, fixedTooltip},
  router,
  render: h => h(App)
})
