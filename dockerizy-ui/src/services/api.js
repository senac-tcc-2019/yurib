import axios from 'axios'

const API_URL = process.env.API_URL || 'http://docker.izy/api'
// const API_URL = process.env.API_URL || 'api'

export default {

  getApiUrl: API_URL,

  // manage apps

  getApps: async () => {
    const { data } = await axios.get(API_URL + '/apps')
    return data
  },

  getAppByName: async (name) => {
    const { data } = await axios.get(API_URL + '/apps/name/' + name)
    return data
  },

  postApp: async (app) => {
    const data = await axios.post(API_URL + '/apps', app)
    return data
  },

  updateApp: async (app) => {
    const data = await axios.put(API_URL + '/apps/' + app.id, app)
    return data
  },

  deleteApp: async (app) => {
    const data = await axios.delete(API_URL + '/apps/' + app.id)
    return data
  },

  initApp: async (id) => {
    const { data } = await axios.post(API_URL + '/apps/start/' + id)
    return data
  },

  stopApp: async (id) => {
    const { data } = await axios.post(API_URL + '/apps/stop/' + id)
    return data
  },

  refreshApp: async (id) => {
    const { data } = await axios.post(API_URL + '/apps/refresh/' + id)
    return data
  },

  clearApp: async (id) => {
    const { data } = await axios.post(API_URL + '/apps/clear/' + id)
    return data
  },

  appLogs: async (id) => {
    const { data } = await axios.get(API_URL + '/apps/logs/' + id)
    return data
  },

  appCmd: async (id) => {
    const { data } = await axios.get(API_URL + '/apps/cmd/' + id)
    return data
  },

  appBuildLog: async (id) => {
    const { data } = await axios.get(API_URL + '/apps/build-logs/' + id)
    return data
  },

  // manage environments

  addEnv: async (env) => {
    const data = await axios.post(API_URL + '/env/', env)
    return data
  },

  removeEnv: async (id) => {
    const data = await axios.delete(API_URL + '/env/' + id)
    return data
  },

  // manage ports

  addPort: async (port) => {
    const data = await axios.post(API_URL + '/port/', port)
    return data
  },

  removePort: async (id) => {
    const data = await axios.delete(API_URL + '/port/' + id)
    return data
  },

  // manage commands

  addRun: async (env) => {
    const data = await axios.post(API_URL + '/run/', env)
    return data
  },

  removeRun: async (id) => {
    const data = await axios.delete(API_URL + '/run/' + id)
    return data
  },

  // manage repositories

  addRepository: async (repository) => {
    const data = await axios.post(API_URL + '/repository/', repository)
    return data
  },

  removeRepository: async (id) => {
    const data = await axios.delete(API_URL + '/repository/' + id)
    return data
  },

  // manage images

  searchImages: async (name) => {
    const { data } = await axios.post(API_URL + '/hub/search', {name})
    return data
  },

  searchTags: async (image, search) => {
    console.log(image)
    const { data } = await axios.post(API_URL + '/hub/search/tags', {image_slug: image.slug || image.image, name: search})
    return data
  },

  getFavoritesImages: async () => {
    const { data } = await axios.get(API_URL + '/images/favorites')
    return data
  },

  postFavoriteImage: async (image) => {
    const { data } = await axios.post(API_URL + '/images/favorites', image)
    return data
  }

}
