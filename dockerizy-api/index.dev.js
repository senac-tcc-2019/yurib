// if (!process.env.DEBUG) process.env.DEBUG = '*';
if (!process.env.DB_HOST) process.env.DB_HOST = 'localhost';
if (!process.env.DB_PORT) process.env.DB_PORT = 32768;
if (!process.env.DB_NAME) process.env.DB_NAME = 'dockerizy';

// if (!process.env.SOCKET_PATH) process.env.SOCKET_PATH = '//./pipe/docker_engine';

require('./index');
