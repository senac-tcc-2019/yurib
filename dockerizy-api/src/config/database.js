module.exports = {
	username: 'dockerizy',
	password: 'dockerizy',
	database: 'dockerizy',
	host: '127.0.0.1',
	dialect: 'mysql',
	port: 3307,
	logging: false
}