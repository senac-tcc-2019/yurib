const {
	Service
} = require('@model');

module.exports.getService = async (req, res) => {
	try {
		const service = await Service.findByPk(req.params.id, {
			include: {
				all: true,
				nested: true
			}
		});
		if (service) res.send(service)
		else res.status(404).end()
	} catch (error) {
		res.status(400).end()
	}
};

module.exports.getServices = async (req, res) => {
	try {
		const services = await Service.findAll({
			include: {
				all: true,
				nested: true
			}
		})
		res.send(services)
	} catch (error) {
		res.status(500).send(error);
	}
};