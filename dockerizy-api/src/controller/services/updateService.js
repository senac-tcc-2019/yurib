const { Service } = require('@model');

module.exports.putService = async (req, res) => {
	try {
		const updated = await Service.update(req.body, { where: { id: req.params.id } })
		res.send(updated);
	} catch (error) {
		res.status(400).send(error)		
	}
};
