const { Service } = require('@model');

module.exports.newService = async (req, res) => {
	if (req.body) {
		try {
			const service = await Service.create(req.body);
			res.send(service);
		} catch (error) {
			return res.status(400).send(error)
		}
	} else {
		res.status(400).send('Invalid input');
	}
};