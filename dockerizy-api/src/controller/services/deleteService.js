const { Service } = require('@model');

module.exports.deleteService = async (req, res) => {
	try {
		const removed = await Service.destroy({ where: { id: req.params.id } })
		if (removed) {
			res.end()
		} else {
			res.status(404).end()
		}
	} catch (error) {
		res.status(500).end()		
	}
};
