const { App, Repository } = require('@model');
const appsService = require('../../services/appsService')
const dockerFileGenerator = require('./../../services/files/dockerfile')
const compose = require('../../services/docker-compose/compose')
const rimraf = require("rimraf")
const hosts = require('../../util/hosts')

module.exports.deleteRepository = async (req, res) => {
	try {
		const repository = await Repository.findOne({
			where: {
				id: req.params.id
			},
			include: { all: true, nested: true }
		})

		const removed = await repository.destroy()

		if (removed) {
			try {
				const app = await App.findOne({
					where: {
						id: repository.AppId,
					},
					include: { all: true, nested: true }
				});

				
				const appRoot = __dirname + '/../../../apps/' + app.name + '/dockerizy/'
				rimraf.sync(appRoot + '/repositories/' + repository.name);
				
				hosts.removeAppCodeRepoDomain(app.name, repository.name)

				// await appsService.recreateAppContainer(app)
				try {
					appsService.setStatus.processing(app)
					await dockerFileGenerator.generateDockerFilesFor(app)
					// compose.up(app)
					// 	.catch(err => {
					// 		console.log('START CONTAINER FAILED:', err)
					// 		appsService.setStatus.error(app)
					// 	})
					// 	.then(out => {
					appsService.setStatus.running(app)
					// 		console.log('CONTAINER STARTED.')
					// 	})
				} catch (error) {
					console.log('ERROR generate Docker Files', error)	
				}
	
			} catch (error) {
				console.log('Error recreating container', error)
			}
			res.end()
		} else {
			res.status(404).end()
		}

	} catch (error) {
		res.status(500).end()
	}
};