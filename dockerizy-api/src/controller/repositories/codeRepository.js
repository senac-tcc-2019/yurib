const { App, Repository } = require('@model');
const compose = require('../../services/docker-compose/compose')

const codeServerContainerPrefix = 'dockerizy_code-server_repo-' //:repoId

module.exports.getCodeRepository = async (appName, repoName) => {
	try {
			
		const app = await App.findOne({
			where: {
				name: appName,
			},
			include: {
				all: true,
				nested: true
			}
		});

		const repository = app.Repositories.find(repo => repo.name == repoName)

		try {
			console.log('trying to up code-server container...')
			await compose.codeServerPath(app.name, repository.name, repository.id)
			console.log('new code-server container is running!')
		} catch (error) {
			console.log('failed to up a new code-server container', error)
		}

		console.log('getting port of requested code-server container...')
		const ports = await compose.getPortsOfContainer(codeServerContainerPrefix + repository.id)
		const codeServerHostPort = ports.find(port => port.container == 8080).host

		const repoDomain = `${app.name}.${repository.name}.code.${process.env.HOST_DOMAIN}`
		return 'http://localhost:' + codeServerHostPort;

	} catch (error) {
		console.log('FAILED TO GET CODE-SERVER URL')
		console.log(error)
	}
};

module.exports.codeRepository = async (req, res, expressApp, appName, repoName) => {
	if (req.body) {
		try {
			
			const app = await App.findOne({
				where: {
					name: appName,
				},
				include: {
					all: true,
					nested: true
				}
			});

			const repository = app.Repositories.find(repo => repo.name == repoName)

			try {
				console.log('trying to up code-server container...')
				await compose.codeServerPath(app.name, repository.name, repository.id)
			} catch (error) {
				console.log('failed to up a new code-server container')
			}

			console.log('getting port of requested code-server...')
			const ports = await compose.getPortsOfContainer(codeServerContainerPrefix + repository.id)
			const codeServerHostPort = ports.find(port => port.container == 8080).host

			console.log('proxying ', codeServerHostPort)

			const repoDomain = `${app.name}.${repository.name}.code.${process.env.HOST_DOMAIN}`
			expressApp.proxy.register(repoDomain, 'http://localhost:' + codeServerHostPort);

			res.status(301).redirect(repoDomain)
		} catch (error) {
			console.log(error)
			res.status(400).send(error)
		}
	} else {
		res.status(400).send('Invalid input');
	}
};