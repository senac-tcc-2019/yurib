const { App, Repository, Run } = require('@model');
const appsService = require('../../services/appsService')
const dockerFileGenerator = require('./../../services/files/dockerfile')
const compose = require('../../services/docker-compose/compose')
const hosts = require('../../util/hosts')

module.exports.newRepository = async (req, res) => {
	if (req.body) {
		try {
			let app = await App.findOne({
				where: {
					id: req.body.AppId, 
				},
				include: { all: true, nested: true }
			});
	
			if (!app) res.status(404).end()

			const repository = await Repository.create(req.body);
			await repository.setApp(app)

			app = await App.findOne({
				where: {
					id: req.body.AppId, 
				},
				include: { all: true, nested: true }
			});

			hosts.addAppCodeRepoDomain(app.name, repository.name)
			
			// await appsService.recreateAppContainer(app)
			try {
				appsService.setStatus.processing(app)
				let higherOrder = 0
				if (app.Runs && app.Runs.length > 0)
					higherOrder = app.Runs.sort((a, b) => {
						if (a.order > b.order) return -1
						if (a.order == b.order) return 0
						if (a.order < b.order) return 1
					})[0].order + 1
				const run = await Run.create(
					{
						AppId: app.id,
						"run": `@COPY ./repositories/${repository.name} ${repository.target}\n`,
						"dir": repository.target,
						"order": higherOrder
					}
				);
				await run.setApp(app)

				app = await App.findOne({
					where: {
						id: req.body.AppId, 
					},
					include: { all: true, nested: true }
				});

				await dockerFileGenerator.generateDockerFilesFor(app)
				// compose.up(app)
				// 	.catch(err => {
				// 		console.log('START CONTAINER FAILED:', err)
				// 		appsService.setStatus.error(app)
				// 	})
				// 	.then(out => {
				appsService.setStatus.running(app)
				// 		console.log('CONTAINER STARTED.')
				// 	})
			} catch (error) {
				console.log('ERROR generate Docker Files', error)	
			}

			res.send(app);
		} catch (error) {
			console.log(error)
			res.status(400).send(error)
		}
	} else {
		res.status(400).send('Invalid input');
	}
};