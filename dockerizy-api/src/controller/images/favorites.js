const { Image } = require('@model');
const listImages = require('@docker/listImages')

module.exports.favorites = async (req, res) => {
	try {
		const dbImages = await Image.findAll();

		const favorites = dbImages.filter(el => el.favorite)

		const dockerImages = await listImages()

		const result = favorites.map(fav => {
			const available = dockerImages && dockerImages.some(img => img.RepoTags && img.RepoTags.some(repo => repo == fav.name + ':' + fav.tag))
			return { ...fav.dataValues, available }
		})

		if (result) res.send(result)
		else res.status(404).end()
	} catch (error) {
		console.log(error)
		res.status(400).send(error)
	}
};