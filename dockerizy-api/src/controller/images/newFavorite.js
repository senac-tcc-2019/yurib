const { Image } = require('@model');

module.exports.newFavorite = async (req, res) => {
	try {
		const dbImages = await Image.findAll();

		const favorites = dbImages.filter(el => el.favorite)

		const alrdyHaveThisFavorite = favorites.some(fav => req.body.slug == fav.slug)

		if (alrdyHaveThisFavorite) {
			return res.status(400).send('You already have this as favorite.')
		}

		req.body.favorite = true
		
		const createdFavorite = await Image.create(req.body)

		res.send(createdFavorite)

	} catch (error) {
		console.log(error)
		res.status(400).send(error)
	}
};
