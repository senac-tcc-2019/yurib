const listImages = require('@docker/listImages')

module.exports.list = async (req, res) => {
	try {
		const images = await listImages()
		res.send(images)		
	} catch (error) {
		res.status(error.statusCode).send(error)
	}
};