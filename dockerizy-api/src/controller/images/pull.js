const pullImage = require('@docker/pullImage')

module.exports.pull = (req, res) => {
	pullImage(req.body.fromImage, (err, stream) => {
		console.log('ERROR:', err)
		if(err) return res.status(err.statusCode).send(err)
		
		console.log("[ starting pull " + req.body.fromImage + " from DockerHub.com ...")
		stream.on('data', function(data) {
			try {
				console.log(JSON.parse(data.toString().trim()).status)
			} catch (error) {
				console.log(data.toString().trim())
			}
			res.write(data);
		});
	  
		stream.on('end', function() {
			console.log("... pull of "+ req.body.fromImage + " finished ]")
		  res.end();
		});
	})
};