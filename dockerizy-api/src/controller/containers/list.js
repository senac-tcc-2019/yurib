const listContainers = require('@docker/listContainers')

module.exports.list = async (req, res) => {
	try {
		const containers = await listContainers()
		res.send(containers)
	} catch (error) {
		console.log('3RR0R', error)
		res.status(error.statusCode || 500).send(error)
	}
};