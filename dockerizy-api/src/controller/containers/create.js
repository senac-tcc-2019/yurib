const createContainer = require('@docker/createContainer')

module.exports.create = async (req, res) => {
	try {
		const response = await createContainer(
			req.body.image,
			req.body.container_name,
			req.body.run,
		 )
		// response.attach({
		// 	stream: true,
		// 	stdout: true,
		// 	stderr: true,
		// 	tty: true
		//   }, function(err, stream) {
		// 	stream.pipe(process.stdout);
		// });
		res.send(response)
	} catch (error) {
		 res.status(error.statusCode).send(error)
	}
};