const { Port, App } = require('@model');
const appsService = require('../../services/appsService')
const dockerFileGenerator = require('../../services/files/dockerfile')
const compose = require('../../services/docker-compose/compose')
const hosts = require('@util/hosts')

module.exports.deletePort = async (req, res) => {
	try {
		const port = await Port.findOne({
			where: {
				id: req.params.id
			},
			include: { all: true, nested: true }
		})

		const removed = await port.destroy()

		if (removed) {
			try {
				const app = await App.findOne({
					where: {
						id: port.AppId,
					},
					include: { all: true, nested: true }
				});

				hosts.removeAppServiceDomain(app.name, port.name)

				// await appsService.recreateAppContainer(app)
				try {
					appsService.setStatus.processing(app)
					await dockerFileGenerator.generateDockerFilesFor(app)
					// compose.up(app)
					// 	.catch(err => {
					// 		console.log('START CONTAINER FAILED:', err)
					// 		appsService.setStatus.error(app)
					// 	})
					// 	.then(out => {
					appsService.setStatus.running(app)
					// 		console.log('CONTAINER STARTED.')
					// 	})
				} catch (error) {
					console.log('ERROR generate Docker Files', error)	
				}
			} catch (error) {
				console.log('Error recreating container', error)
			}
			res.end()
		} else {
			res.status(404).end()
		}

	} catch (error) {
		res.status(500).end()
	}
};