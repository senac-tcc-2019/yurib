const { App, Run } = require('@model');
const appsService = require('../../services/appsService')
const dockerFileGenerator = require('../../services/files/dockerfile')
const compose = require('../../services/docker-compose/compose')

module.exports.newRun = async (req, res) => {
	if (req.body) {
		try {
			let app = await App.findOne({
				where: {
					id: req.body.AppId, 
				},
				include: { all: true, nested: true }
			});
	
			if (!app) res.status(404).end()

			const run = await Run.create(req.body);
			await run.setApp(app)

			app = await App.findOne({
				where: {
					id: req.body.AppId, 
				},
				include: { all: true, nested: true }
			});

			// await appsService.recreateAppContainer(app)
			try {
				appsService.setStatus.processing(app)
				await dockerFileGenerator.generateDockerFilesFor(app)
				// compose.up(app)
				// 	.catch(err => {
				// 		console.log('START CONTAINER FAILED:', err)
				// 		appsService.setStatus.error(app)
				// 	})
				// 	.then(out => {
				appsService.setStatus.running(app)
				// 		console.log('CONTAINER STARTED.')
				// 	})
			} catch (error) {
				console.log('ERROR generate Docker Files', error)	
			}

			res.send(app);
		} catch (error) {
			console.log(error)
			res.status(400).send(error)
		}
	} else {
		res.status(400).send('Invalid input');
	}
};