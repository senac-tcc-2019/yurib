const { Run, App } = require('@model');
const appsService = require('../../services/appsService')
const dockerFileGenerator = require('../../services/files/dockerfile')
const compose = require('../../services/docker-compose/compose')

module.exports.deleteRun = async (req, res) => {
	try {
		const run = await Run.findOne({
			where: {
				id: req.params.id
			},
			include: { all: true, nested: true }
		})

		const removed = await run.destroy()

		if (removed) {
			try {
				const app = await App.findOne({
					where: {
						id: run.AppId,
					},
					include: { all: true, nested: true }
				});

				// await appsService.recreateAppContainer(app)
				try {
					appsService.setStatus.processing(app)
					await dockerFileGenerator.generateDockerFilesFor(app)
					// compose.up(app)
					// 	.catch(err => {
					// 		console.log('START CONTAINER FAILED:', err)
					// 		appsService.setStatus.error(app)
					// 	})
					// 	.then(out => {
					appsService.setStatus.running(app)
					// 		console.log('CONTAINER STARTED.')
					// 	})
				} catch (error) {
					console.log('ERROR generate Docker Files', error)	
				}
			} catch (error) {
				console.log('Error recreating container', error)
			}
			res.end()
		} else {
			res.status(404).end()
		}

	} catch (error) {
		res.status(500).end()
	}
};