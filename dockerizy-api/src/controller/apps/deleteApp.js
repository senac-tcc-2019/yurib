const { App } = require('@model');
const compose = require('../../services/docker-compose/compose')
const rimraf = require("rimraf")


module.exports.deleteApp = async (req, res) => {
	try {

		const app = await App.findOne({
			where: {
				id: req.params.id,
			},
			include: {
				all: true,
				nested: true
			}
		});

		const removed = await App.destroy({ where: { id: req.params.id } })
		await compose.downApp(app)

		const appRoot = __dirname + '/../../../apps/' + app.name
		rimraf.sync(appRoot);

		if (removed) {
			res.end()
		} else {
			res.status(404).end()
		}
		
	} catch (error) {
		console.log('DELETE APP ERROR', error)
		res.status(500).end()		
	}
};
