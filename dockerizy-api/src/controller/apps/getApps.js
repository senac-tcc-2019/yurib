const { App } = require('@model');
const appsService = require('../../services/appsService')
const compose = require('../../services/docker-compose/compose')

module.exports.getApp = async (req, res) => {
	try {
		const app = await App.findOne({
			where: {
				id: req.params.id,
			},
			include: {
				all: true,
				nested: true
			}
		});

		const status = await appsService.appStatus(app)
		status.state.container = await compose.getStatus(app.name)

		let exposedPorts = {}
		compose.getPorts(app.name)
			.catch(err => {
				// console.log('GET PORTS at GET APP ROUTE FAILS for ', app.name)
				// console.log(err)
			})
			.then(out => {
				exposedPorts = out
			})
			.finally(() => {
				if (app) res.send({
					...app.dataValues,
					status,
					exposedPorts
				})
				else res.status(404).end()
			})
	} catch (error) {
		console.log(error)
		res.status(400).send(error)
	}
};

module.exports.getAppByName = async (req, res) => {
	try {
		const app = await App.findOne({
			where: {
				name: req.params.name
			},
			include: {
				all: true,
				nested: true
			}
		});

		if (!app) return res.status(404).end()

		const status = await appsService.appStatus(app)
		status.container = await compose.getStatus(app.name)
		
		let exposedPorts = {}
		compose.getPorts(app.name)
			.catch(err => {
				// console.log('GET PORTS at GET APP ROUTE FAILS for ', app.name)
			})
			.then(out => {
				exposedPorts = out
			})
			.finally(() => {
				if (app) res.send({
					...app.dataValues,
					status,
					exposedPorts
				})
				else res.status(404).end()
			})
	} catch (error) {
		console.log(error)
		res.status(400).send(error)
	}
};

module.exports.getApps = async (req, res) => {
	try {
		const apps = await App.findAll({
			include: {
				all: true,
				nested: true
			}
		})

		const appsWithStatus = await Promise.all(apps.map(async app => {

			const status = await appsService.appStatus(app)
			status.state.container = await compose.getStatus(app.name)

			let exposedPorts = {}
			return await new Promise((resolve, reject) => {
				compose.getPorts(app.name)
					.catch(err => {
						// console.log('GET PORTS at GET APPS ROUTE FAILS for ', app.name)
					})
					.then(out => {
						exposedPorts = out
					})
					.finally(() => {
						resolve({
							...app.dataValues,
							status,
							exposedPorts
						})
					})
			})
		}))

		res.send(appsWithStatus)
	} catch (error) {
		res.status(500).send(error);
	}
};