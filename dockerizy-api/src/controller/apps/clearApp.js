const { App } = require('@model');
const appsService = require('../../services/appsService')
const compose = require('../../services/docker-compose/compose')
module.exports.clearApp = async (req, res) => {
	try {
		// GET APP
		const app = await App.findOne({
			where: {
				id: req.params.id, 
			},
			include: { all: true, nested: true }
		});
	
		// await appsService.stopAppContainer(app)
		// await appsService.recreateAppContainer(app)
		try {
			appsService.setStatus.removingContainer(app)
			compose.down(app)
				.catch(err => {
					console.log('DOWN CONTAINER FAILED:', err)
					appsService.setStatus.error(app)
				})
				.then(out => {
					appsService.setStatus.stopped(app)
					console.log('CONTAINER DOWN.')
				})
		} catch (error) {
			console.log('ERROR generate Docker Files', error)	
		}

		res.end()

	} catch (error) {
		console.error(error)
		return res.status(error.statusCode || 500).send(error)
	}
};