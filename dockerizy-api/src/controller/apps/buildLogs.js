// const logsContainer = require('@docker/logsContainer')
const {
	App
} = require('@model');
const compose = require('../../services/docker-compose/compose')

module.exports.buildLogs = async (req, res) => {
	try {
		const app = await App.findByPk(req.params.id, {
			include: {
				all: true,
				nested: true
			}
		})
		// const container = await appsService.getDockerContainerOf(app)
		// logsContainer('dockerizy_'+app.name)
		try {
			const logs = await compose.getBuildLogs(app.name)
			res.send(logs)
		} catch (error) {
			console.log('GET BUILD LOGS FAIL', error)
			res.status(500).send(error)
		}

	} catch (error) {
		console.log('LOGS ERROR', error)
		res.status(error.statusCode || 500).send(error)
	}
};