const { App } = require('@model');
const compose = require('../../services/docker-compose/compose')

module.exports.getCmd = async (req, res) => {
	try {
		const app = await App.findByPk(req.params.id, {
			include: {
				all: true,
				nested: true
			}
		})
		// const container = await appsService.getDockerContainerOf(app)
		// cmdContainer('dockerizy_'+app.name)
		try {
			const cmd = await compose.getCmd(app)
			res.send(cmd)
		} catch (error) {
			// console.log('GET CMD FAIL', error)
			res.status(500).send(error)
		}

	} catch (error) {
		console.log('LOGS ERROR', error)
		res.status(error.statusCode || 500).send(error)
	}
};