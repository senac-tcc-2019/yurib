const { App } = require('@model');
const dockerFileGenerator = require('./../../services/files/dockerfile')
const compose = require('../../services/docker-compose/compose')
const appsService = require('../../services/appsService')

module.exports.newApp = async (req, res) => {
	if (req.body) {
		// remove docker invalid characters
		req.body.name && (req.body.name = req.body.name.replace(new RegExp('[^a-zA-Z0-9\\._\\-]', 'g'), ''))

		try {
			const newApp = await App.create(req.body);

			const app = await App.findOne({
				where: {
					id: newApp.id,
				},
				include: {
					all: true,
					nested: true
				}
			});

			try {
				appsService.setStatus.initializing(app)
				dockerFileGenerator.generateDockerFilesFor(app)
				compose.up(app)
					.catch(err => {
						console.log('UP NEW APP FAIL', err)
						appsService.setStatus.error(app)
					})
					.then(async out => {
						appsService.setStatus.running(app)
						console.log('CONTAINER UP')
						console.log(out)
						let ports = []
						compose.getPorts(app.name)
							.catch(err => {
								console.log('GET PORTS AT APP CREATING FAILS', err)
							})
							.then(out => {
								ports = out
							})
							.finally(async () => {
								if (ports.length > 0) {
									console.log('Binding ', app.name, 'ports:', ports)
									appsService.setStatus.bindingPorts(app)
									await app.update({
										ports: ports.map(port => port.container)
									})
									dockerFileGenerator.generateDockerFilesFor(app)
									compose.up(app)
										.catch(err => {
											appsService.setStatus.error(app)
											console.log('UP NEW APP WITH PORTS FAIL', err)
										})
										.then(out => {
											appsService.setStatus.running(app)
											console.log('APP UP WITH EXPOSED PORTS')
											console.log(out)
										})
								}
							})
					})
			} catch (error) {
				console.log('ERROR generate Docker Files', error)
			}

			res.send(app);
		} catch (error) {
			console.log('NEW APP ERROR', error)
			return res.status(400).send(error)
		}
	} else {
		console.log('INVALID INPUT ERROR')
		res.status(400).send('Invalid input');
	}
};