const { App } = require('@model');
const appsService = require('../../services/appsService')
const dockerFileGenerator = require('./../../services/files/dockerfile')
const compose = require('../../services/docker-compose/compose')

module.exports.putApp = async (req, res) => {
	try {
		const app = await App.findOne({
			where: {
				id: req.params.id
			},
			include: {
				all: true,
				nested: true
			}
		})
		const updatedApp = await app.update(req.body, { returning: true, plain: true })
		
		// await appsService.updateAppContainer(updatedApp)
		try {
			appsService.setStatus.processing(app)
			dockerFileGenerator.generateDockerFilesFor(app)
			// compose.up(app)
			// 	.catch(err => {
			// 		console.log('START CONTAINER FAILED:', err)
			// 		appsService.setStatus.error(app)
			// 	})
			// 	.then(out => {
			appsService.setStatus.running(app)
			// 		console.log('CONTAINER STARTED.')
			// 	})
		} catch (error) {
			console.log('ERROR generate Docker Files', error)	
		}

		res.send(updatedApp);
	} catch (error) {
		console.log(error)
		res.status(400).send(error)
	}
};