const { App } = require('@model');
const appsService = require('../../services/appsService')
const dockerFileGenerator = require('./../../services/files/dockerfile')
const compose = require('../../services/docker-compose/compose')

module.exports.refreshApp = async (req, res) => {
	try {
		// GET APP
		const app = await App.findOne({
			where: {
				id: req.params.id, 
			},
			include: { all: true, nested: true }
		});
	
		// await appsService.recreateAppContainer(app)
		try {
			appsService.setStatus.processing(app)
			dockerFileGenerator.generateDockerFilesFor(app)
			compose.refresh(app)
				.catch(err => {
					console.log('START CONTAINER FAILED:', err)
					appsService.setStatus.error(app)
				})
				.then(out => {
					appsService.setStatus.running(app)
					console.log('CONTAINER STARTED.')
				})
		} catch (error) {
			console.log('ERROR generate Docker Files', error)	
		}


		res.end()

	} catch (error) {
		console.error(error)
		return res.status(error.statusCode || 500).send(error)
	}
};