const { App } = require('@model');
const appsService = require('../../services/appsService')
const compose = require('../../services/docker-compose/compose')
module.exports.startApp = async (req, res) => {
	try {
		// GET APP
		const app = await App.findOne({
			where: {
				id: req.params.id, 
			},
			include: { all: true, nested: true }
		});
	
		// await appsService.stopAppContainer(app)
		// await appsService.recreateAppContainer(app)
		try {
			appsService.setStatus.initializing(app)
			compose.start(app)
				.catch(err => {
					console.log('START CONTAINER FAILED:', err)
					appsService.setStatus.error(app)
				})
				.then(out => {
					appsService.setStatus.stopped(app)
					console.log('CONTAINER STARTED.')
				})
		} catch (error) {
			console.log('ERROR generate Docker Files', error)	
		}

		res.end()

	} catch (error) {
		console.error(error)
		return res.status(error.statusCode || 500).send(error)
	}
};