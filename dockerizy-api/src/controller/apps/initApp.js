const {
	App
} = require('@model');
const startContainer = require('@docker/startContainer')
const appsService = require('../../services/appsService')
const compose = require('../../services/docker-compose/compose')

global.appsState = {}

module.exports.initApp = async (req, res) => {
	try {
		// GET APP
		const app = await App.findOne({
			where: {
				id: req.params.id,
			},
			include: {
				all: true,
				nested: true
			}
		});

		try {

			if (!app) return res.status(404).end()

			// VERIFY IF APP IS ALRDY INITIALIZING BY ANOTHER REQUEST
			if ((await appsService.appStatus(app)).processing)
				return res.status(200).send(await appsService.appStatus(app))

			console.log('[[ START INITIALIZE APP ]]')

			appsService.setStatus.initializing(app)

			compose.up(app)
				.catch(err => {
					console.log('START CONTAINER FAILED:', err)
					appsService.setStatus.error(app)
					return res.status(err.statusCode || 500).send(err)
				})
				.then(out => {
					appsService.setStatus.running(app)
					console.log('CONTAINER STARTED.')
					return res.send(appsService.appStatus(app))
				})
		} catch (error) {
			console.error(error)
			appsService.setStatus.error(app)
			if (!res.headersSent) res.status(error.statusCode || 500).send(error)
		}
	} catch (error) {
		console.error(error)
		return res.status(error.statusCode || 500).send(error)
	}
}

module.exports.initApp2 = async (req, res) => {
	try {
		// GET APP
		const app = await App.findOne({
			where: {
				id: req.params.id,
			},
			include: {
				all: true,
				nested: true
			}
		});

		try {

			if (!app) return res.status(404).end()

			// VERIFY IF APP IS ALRDY INITIALIZING BY ANOTHER REQUEST
			if ((await appsService.appStatus(app)).processing)
				return res.status(200).send(await appsService.appStatus(app))

			console.log('[[ START INITIALIZE APP ]]')
			console.log('SEARCHING CONTAINER...')
			// LIST CONTAINERS AND VERIFY IF CONTAINER OF APP EXISTS
			const foundContainer = await appsService.getDockerContainerOf(app)

			// IF CONTAINER EXISTS, START AND DONE.
			if (foundContainer) {
				console.log('CONTAINER FOUND.')
				if (foundContainer.State != 'running') {
					console.log('STARTING CONTAINER...')
					appsService.setStatus.initializing(app)
					startContainer(foundContainer.Id, (err, result) => {
						if (err) {
							console.log('START CONTAINER FAILED:', err)
							return res.status(err.statusCode || 500).send(err)
						}
						appsService.setStatus.running(app)
						console.log('CONTAINER STARTED.')
						return res.send(appsService.appStatus(app))
					})
				} else {
					appsService.setStatus.running(app)
					console.log('CONTAINER ALREADY STARTED.')
					return res.send(appsService.appStatus(app))
				}
			} else {
				// ELSE
				// LIST DOCKER IMAGES AND VERIFY IF HOST HAS THE APP IMAGE FOR CREATE A NEW CONTAINER
				console.log('VERIFY IF APP IMAGE EXISTS ON HOST...')
				const imageAvailable = await appsService.isImageAvailable(app)

				// IF HOST DOESNT HAVE APP IMAGE, PULL THEN
				if (!imageAvailable) {
					console.log('IMAGE NOT FOUND.')
					appsService.pullImageOf(app)
					appsService.setStatus.pullingImage(app)
					res.send(appsService.appStatus(app))
				}
				// AFTER CONFIRMATION THAT HOST HAVE THE APP IMAGE, CREATE AND START A NEW CONTAINER
				const createdContainer = await appsService.createAppContainer(app)
				await createdContainer.start()
				appsService.setStatus.running(app)
			}
		} catch (error) {
			console.error(error)
			appsService.setStatus.error(app)
			if (!res.headersSent) res.status(error.statusCode || 500).send(error)
		}
	} catch (error) {
		console.error(error)
		return res.status(error.statusCode || 500).send(error)
	}
};