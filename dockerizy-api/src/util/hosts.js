const hostile = require('hostile');
const { App: AppModel } = require('@model');
const dockerizyLocalDomain = 'docker.izy'

module.exports.addLocalDomain = (domain) => {
	hostile.set('127.0.0.1', domain, function (err) {
		if (err) {
			console.error(err)
		} else {
			console.log('set /etc/hosts', domain, 'successfully!')
		}
	})
}

module.exports.addAppServiceDomain = (appName, serviceName) => {
	hostile.set('127.0.0.1', appName + '.' + serviceName + '.' + dockerizyLocalDomain, function (err) {
		if (err) {
			console.error(err)
		} else {
			console.log('set /etc/hosts', appName + '.' + serviceName + '.' + dockerizyLocalDomain, 'successfully!')
		}
	})
}

module.exports.addAppCodeRepoDomain = (appName, repoName) => {
	hostile.set('127.0.0.1', appName + '.' + repoName + '.code.' + dockerizyLocalDomain, function (err) {
		if (err) {
			console.error(err)
		} else {
			console.log('set /etc/hosts', appName + '.' + repoName + '.code.' + dockerizyLocalDomain, 'successfully!')
		}
	})
}

module.exports.removeAppCodeRepoDomain = (appName, repoName) => {
	hostile.remove('127.0.0.1', appName + '.' + repoName + '.code.' + dockerizyLocalDomain, function (err) {
		if (err) {
			console.error(err)
		} else {
			console.log('set /etc/hosts', appName + '.' + repoName + '.' + dockerizyLocalDomain, 'successfully!')
		}
	})
}

module.exports.removeAppServiceDomain = (appName, serviceName) => {
	hostile.remove('127.0.0.1', appName + '.' + serviceName + '.' + dockerizyLocalDomain, function (err) {
		if (err) {
			console.error(err)
		} else {
			console.log('set /etc/hosts', appName + '.' + serviceName + '.' + dockerizyLocalDomain, 'successfully!')
		}
	})
}

module.exports.removeLocalDomain = (domain) => {
	hostile.remove('127.0.0.1', domain, function (err) {
		if (err) {
			console.error(err)
		} else {
			console.log('set /etc/hosts', domain, 'successfully!')
		}
	})
}

module.exports.setDockerizyDomain = () => {
	this.addLocalDomain(dockerizyLocalDomain)
}

module.exports.createAppsDomain = async () => {
	const apps = await AppModel.findAll({
		include: {
			all: true,
			nested: true
		}
	})
	apps.forEach(app => {
		app.Ports.forEach(port => {
			this.addAppServiceDomain(app.name, port.name)
		})
		app.Repositories.forEach(repo => {
			this.addAppCodeRepoDomain(app.name, repo.name)
		})
	});
}

module.exports.localDomain = dockerizyLocalDomain