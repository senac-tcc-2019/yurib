const axios = require('axios');

const HUB_URL = 'https://hub.docker.com/'

module.exports.getRecommendedImages = async () => {
	const {
		data
	} = await axios.get('https://kitematic.com/recommended.json');
	data.repos = data.repos.map(el => ({
		...el,
		img: 'https://kitematic.com/recommended/' + el.img
	}))
	return data
};

module.exports.searchImage = async ({
	name = '',
	page = 1,
	paginate = 10
}) => {
	const verifiedItemsUrl = 'api/content/v1/products/search?image_filter=store%2Cofficial&order=desc&sort=popularity&q='
	const communityItemsUrl = 'api/content/v1/products/search?source=community&order=desc&sort=popularity&q='

	const itens = []

	const {
		data: verified
	} = await axios.get(
		HUB_URL + verifiedItemsUrl +
		name +
		'&page=' +
		page +
		'&page_size=' +
		(name.length > 0 ? paginate / 2 : paginate)
	)
	verified.summaries && itens.push(...verified.summaries)

	if (name.length > 0) {
		const {
			data: community
		} = await axios.get(
			HUB_URL + communityItemsUrl +
			name +
			'&page=' +
			page +
			'&page_size=' +
			(paginate / 2)
		)
		community.summaries && itens.push(...community.summaries)
	}

	return itens.length > 0 && itens.map(el => {
		return {
			name: el.name,
			slug: el.slug ? el.slug : el.name,
			description: el.short_description,
			logoUrl: el.logo_url && (el.logo_url.large || el.logo_url.small),
			favorite: false,
			popularity: el.popularity
		}
	})
}


module.exports.getTagsFrom = async ({
	image_slug,
	name,
	page = 1,
	paginate = 15
}) => {
	try {
		const {
			data
		} = await axios.get(
			HUB_URL + 'v2/repositories/library/' +
			image_slug +
			'/tags/?' +
			'page=' + page +
			'&page_size=' + paginate +
			'&name=' + name +
			'&ordering=last_updated'
		)
		return data && data.results.map(el => {
			return {
				name: el.name,
				size: el.full_size,
			}
		})
	} catch (error) {
		console.log('ERROR:', error)
		console.log('Trying another route...')
		try {
			const {
				data
			} = await axios.get(
				HUB_URL + 'v2/repositories/' +
				image_slug +
				'/tags/?' +
				'page=' + page +
				'&page_size=' + paginate +
				'&name=' + name +
				'&ordering=last_updated'
			)
			return data && data.results.map(el => {
				return {
					name: el.name,
					size: el.full_size,
				}
			})
		} catch (error) {
			console.log('ERROR:', error)
			return new Error(error)
		}
	}
}