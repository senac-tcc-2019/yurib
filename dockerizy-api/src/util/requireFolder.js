module.exports.fromDir = (__dirname) => {
	const normalizedPath = require("path").join(__dirname);
	require("fs").readdirSync(normalizedPath).sort((a, b) => {
		if (a > b) return 1
		if (a == b) return 0
		if (a < b) return -1
	}).forEach(function(file) {
		if(file != 'index.js') require(normalizedPath + "/" + file);
	});
}

module.exports.fromDirAndReturn = (__dirname) => {
	const normalizedPath = require("path").join(__dirname);
	let result = {}
	require("fs").readdirSync(normalizedPath).sort((a, b) => {
		if (a > b) return 1
		if (a == b) return 0
		if (a < b) return -1
	}).forEach(function(file) {
		if(file != 'index.js') {
			console.log('requiring...', normalizedPath + "/" + file)
			result = { ...result, ...require(normalizedPath + "/" + file) }
		};
	});
	return result
}

module.exports.withParam = (__dirname, param) => {
	const normalizedPath = require("path").join(__dirname);
	require("fs").readdirSync(normalizedPath).sort((a, b) => {
		if (a > b) return 1
		if (a == b) return 0
		if (a < b) return -1
	}).forEach(function(file) {
		if(file != 'index.js') {
			console.log('requiring...', normalizedPath + "/" + file)
			require(normalizedPath + "/" + file)(param)
		};
	});
}
