const { spawn } = require('child_process');

module.exports.chmodR777 = async (path) => {
	return new Promise((resolve, reject) => {
		// console.log('chmod -R 777', path, '...')
		const out = spawn('chmod', ['-R', '777', path]);

		let result = ''
		let err = ''

		out.stdout.on('data', (data) => {
			result += data
		});

		out.stderr.on('data', (data) => {
			err += data
		});

		out.on('close', async (code) => {
			if (!code) {
				// console.log('CHMOD 777 -R', path, '->', result)
				resolve(result.replace('\n', ''))
			} else {
				console.log('CHMOD 777 -R', path, '-> ERROR', err)
				reject(err)
			}
		});
	})
}

module.exports.pwd = async () => {
	return new Promise((resolve, reject) => {
		const out = spawn('pwd');

		let result = ''
		let err = ''

		out.stdout.on('data', (data) => {
			result += data
		});

		out.stderr.on('data', (data) => {
			err += data
		});

		out.on('close', async (code) => {
			if (!code) {
				console.log('PWD ->', result)
				resolve(result.replace('\n', ''))
			} else {
				console.log('run pwd >>> ERROR', err)
				reject(err)
			}
		});
	})
}

module.exports.userId = async () => {
	return new Promise((resolve, reject) => {
		const out = spawn('id', ['-u']);

		let result = ''
		let err = ''

		out.stdout.on('data', (data) => {
			result += data
		});

		out.stderr.on('data', (data) => {
			err += data
		});

		out.on('close', async (code) => {
			if (!code) {
				console.log('USER ID ->', result)
				resolve(result.replace('\n', ''))
			} else {
				console.log('run id -u >>> ERROR', err)
				reject(err)
			}
		});
	})
}