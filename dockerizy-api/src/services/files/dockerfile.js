const fs = require('fs')
const yaml = require('node-yaml');
const mkdirp = require('mkdirp');
const git = require('../git')
const exec = require('../../util/exec')

let tempLastWorkingDirUsed =  null

function getNextWoringkDir(dir) {
	if (tempLastWorkingDirUsed == dir || !dir) return ''
	tempLastWorkingDirUsed = dir
	return `WORKDIR ${dir}\n`
}

function makeDockerFileObjectWith(service) {
	const dockerfile =
		`FROM ${service.image + ':' + service.tag}\n` +
		// `USER root\n` +
		// `${service.Repositories ? service.Repositories.map(repo => `COPY ./repositories/${repo.name} ${repo.target}\n`).join('\n') : ''}` +
        `${service.Envs ? service.Envs.map(env => 'ENV ' + env.key + ' ' + env.value).join('\n') + (service.Envs.length > 0 ? '\n' : '') : ''}` +
		`${service.Runs ? service.Runs.map(run => getNextWoringkDir(run.dir) + (run.run.startsWith('@') ? run.run.substring(1) : 'RUN ' + run.run)).join('\n') + (service.Runs.length > 0 ? '\n' : '') : ''}` +
        `${service.Ports ? service.Ports.length > 0 ? 'EXPOSE ' + service.Ports.map(port => port.port).join(' ') + '\n' : '' : ''}` +
        `${service.cmd ? getNextWoringkDir(service.cmd.split(' -/- ')[0]) + 'CMD ' + service.cmd.split(' -/- ')[1] + '\n' : ''}`
	// `${service.cmd ? service.cmd ? 'CMD [\"' + service.cmd.split(' ').join('\", \"') + '\"]\n' : '' : ''}`
	console.warn(dockerfile)
	tempLastWorkingDirUsed = null
	return dockerfile
}

function mkdirp777(path) {
	mkdirp.sync(path)
	exec.chmodR777(path)
}

module.exports = {

	generateDockerFilesFor: async (app) => {
		// APP Dockerfile
		const Dockerfile = makeDockerFileObjectWith(app)
		const appRoot = __dirname + '/../../../apps/' + app.name + '/dockerizy/'

		mkdirp777(appRoot)

		fs.writeFileSync(appRoot + '/Dockerfile', Dockerfile);

		// Services Dockerfiles
		if (app.Services) app.Services.forEach(service => {

			const serviceDockerfile = makeDockerFileObjectWith(service)

			mkdirp777(appRoot + '/services/' + service.name);

			fs.writeFileSync(appRoot + '/services/' + service.name + '/Dockerfile', serviceDockerfile);

		})

		// create /src folder
		// mkdirp777(appRoot + '/src');

		async function getAppVolumes(repositories) {
			const volumes = []
			for (repo of repositories) {
				mkdirp777(appRoot + '/repositories/' + repo.name);
				await git.clone(repo, appRoot + '/repositories/', repo.name)
				await git.checkout(repo.branch, appRoot + '/repositories/' + repo.name)
				// volumes.push({
				// 	type: 'bind',
				// 	source: './repositories/' + repo.name,
				// 	target: repo.target
				// })
			}
			if (volumes.length > 0) {
				return { volumes }
			} else {
				return {}
			}
		}

		async function getServiceVolumes(repositories) {
			const volumes = []
			for (repo of repositories) {
				mkdirp777(appRoot + '/services/' + '/repositories/' + repo.name);
				await git.clone(repo, appRoot + '/repositories/', repo.name)
				await git.checkout(repo.branch, appRoot + '/repositories/' + repo.name)
				// volumes.push({
				// 	type: 'bind',
				// 	source: './repositories/' + repo.name,
				// 	target: repo.target
				// })
			}
			if (volumes.length > 0) {
				return { volumes }
			} else {
				return {}
			}
		}


		// APP docker-compose.yml
		const docker_compose_yml = {
			version: '3.7',
			networks: {
				dockerizy: {
					external: {
						name: 'dockerizy'
					}
				}
			},
			services: {
				[app.name]: {
					build: '.',
					container_name: 'dockerizy_' + app.name,
					...(app.Repositories && app.Repositories.length > 0 && {
						...(await getAppVolumes(app.Repositories))
					}),
					...(app.Ports && app.Ports.length > 0 && {
						ports: app.Ports.map(port => port.port)
					}),
					networks: ['dockerizy']
				},
				...app.Services && await app.Services.reduce(async (prev, curr) => {
					mkdirp777(appRoot + '/services/' + curr.name);
					prev[curr.name] = {
						build: './services/' + curr.name + '/',
						container_name: 'dockerizy_' + curr.name + '_' + curr.name,
						...(curr.Repositories && curr.Repositories.length > 0 && {
							...(await getServiceVolumes(curr.Repositories))
						}),
						...(curr.Ports && curr.Ports.length > 0 && {
							ports: curr.Ports.map(port => port.port)
						}),
						networks: ['dockerizy']

					}
					return prev
				}, Promise.resolve({}))
			},
		}

		// console.log('DOCKER COMPOSE:', docker_compose_yml)

		mkdirp777(appRoot);

		yaml.writeSync(appRoot + '/docker-compose.yml', docker_compose_yml);

		// fs.writeFileSync(appRoot + '/docker-compose3.json', JSON.stringify(docker_compose_yml));

	}

}

// module.exports.generateDockerFilesFor({

//     name: "nodeserver",
//     image: "node",
//     ports: [ 80, 443 ],
//     Envs: [{
//             key: 'DATABASEURL',
//             value: '0.0.0.0'
//         },
//         {
//             key: 'DATABASEURL2',
//             value: '0.0.0.0'
//         },
//         {
//             key: 'DATABASEURL3',
//             value: '0.0.0.0'
//         }
//     ],
//     Services: [{
//         name: "database",
//         image: "mysql",
//         ports: [ 3306 ],
//         Envs: [{
//             key: 'ROOT_PASSWORD',
//             value: 'test'
//         }, ],
//         runs: [],
//         run: null
//     }],
//     runs: [
//         'apt-get update',
//         'apt-get install git',
//     ],
//     run: 'npm start'
// })