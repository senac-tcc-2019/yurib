const git = require('simple-git/promise')();

module.exports.clone = async (appRepository, src, name) => {
    
	const USER = appRepository.user;
	const PASS = appRepository.password;
	const isLocal = appRepository.url.startsWith('/') || appRepository.url.startsWith('./')
	const REPO = appRepository.url.replace('https://', '').replace('http://', '');

	const remote = `https://${USER}:${PASS}@${REPO}`;

	try {
		await git.cwd(src)
		await git.clone(isLocal ? appRepository.url : remote, name)
		// disable file permissions changes detection:
		setTimeout(() => {
			git.raw(
				[
				  'config',
				  '--local',
				  'core.fileMode',
				  'false'
				])
		}, 1000)
		console.log('clone ' + appRepository.name + ' finished')
	} catch (error) {
		console.error('clone ' + appRepository.name + ' failed: ', error)
	}
            
}

module.exports.checkout = async (branch, src) => {

	try {
		await git.cwd(src)
		await git.checkout(branch)
		console.log('checkout to ' + branch + '  success')
	} catch (error) {
		console.error('checkout to ' + branch + ' failed: ', error)
	}
    
}