const updateContainer = require('@docker/updateContainer')
const listContainers = require('@docker/listContainers')
const listImages = require('@docker/listImages')
const getContainerBy = require('@docker/getContainer')
const createContainer = require('@docker/createContainer')
const pullImage = require('@docker/pullImage')

module.exports.setStatus = { // LIST OF POSSIBLE STATUS
	running: 		     (app) => global.appsState[app.id] = { state: "running",                     processing: false },
	notInitialized:      (app) => global.appsState[app.id] = { state: "not initialized",             processing: false },
	stopped:             (app) => global.appsState[app.id] = { state: "stopped",                     processing: false },
	finishProcessing:    (app) => global.appsState[app.id] = { state: "finish processing",           processing: false },
	processing:          (app) => global.appsState[app.id] = { state: "processing...",               processing: true },
	initializing:        (app) => global.appsState[app.id] = { state: "initializing...",             processing: true },
	bindingPorts:        (app) => global.appsState[app.id] = { state: "binding ports...",            processing: true },
	pullingImage:        (app) => global.appsState[app.id] = { state: "pulling image from hub...",   processing: true },
	creatingContainer:   (app) => global.appsState[app.id] = { state: "creating app container...",   processing: true },
	removingContainer:   (app) => global.appsState[app.id] = { state: "removing app container...",   processing: true },
	recreatingContainer: (app) => global.appsState[app.id] = { state: "recreating app container...", processing: true },
	stoppingContainer:   (app) => global.appsState[app.id] = { state: "stopping app container...",   processing: true },
	error:        (app, error) => global.appsState[app.id] = { state: "something bad happened",      processing: false },
}

module.exports.getDockerContainerOf = async (app) => {
	const dockerContainers = await listContainers()
	let foundDockerContainer
	app.containers.find(appContainer => {
		const foundedDockerContainerById = dockerContainers.find(dockerContainer => dockerContainer.Id == appContainer)
		
		if (foundedDockerContainerById) 
		{
			const tagMatches = foundedDockerContainerById.Image.split(':')[1] == app.tag
			if (tagMatches) foundDockerContainer = foundedDockerContainerById
			return tagMatches
		}
		else 
		{
			return false
		}
		
	})
	return foundDockerContainer
}

// TODO (NOT WORKING)
module.exports.updateAppContainer = async (app) => {
	
	const foundContainer = await this.getDockerContainerOf(app)
	
	console.log('updating container...', app)
	await new Promise((resolve, reject) => {
		console.log('new envs', app.Envs.map(el => el.key + "=" + el.value))
		updateContainer(foundContainer.Id, {
			// Image: 'node:8-alpine',
			Image: app.image + ':' + app.tag,
			// name: 'dockerizy-container',
			name: 'dockerizy_' + app.name + '.' + app.tag,
			//Env: [
			// 'PORT=8080'
			// ],
			Env: app.Envs.map(el => el.key + "=" + el.value),
			// Run: ['/bin/bash'],
			Run: app.run
		}, (err, result) => {
			console.log('container updated or not:')
			if (err)
				console.log('ERROR: ' + err)
			console.log('container updated.', result)
			resolve()
		})
		
	})
}

module.exports.createAppContainer = async (app) => {
	console.log('CREATING NEW CONTAINER...')
	this.setStatus.creatingContainer(app)
	const createdContainer = await createContainer(app.image, app.tag, app.name, app.run, app.Envs)
	console.log('CONTAINER CREATED.')
	console.log('SALVING CONTAINER ID ON APP...')
	await app.update({
		containers: [...app.containers, createdContainer.id]
	})
	console.log('CONTAINER SAVED.')
	return createdContainer
}

module.exports.recreateAppContainer = async (app) => {
	console.log('RECREATING APP CONTAINER...')
	this.setStatus.recreatingContainer(app)
	const foundContainerInfo = await this.getDockerContainerOf(app)
	let wasRunning = false
	
	if (foundContainerInfo) {
		console.log(foundContainerInfo.State)
		wasRunning = foundContainerInfo.State == 'running'
		const container = getContainerBy(foundContainerInfo.Id)
		this.setStatus.removingContainer(app)
		await container.remove({
			force: true
		})
		console.log('CONTAINER REMOVED.')
		console.log('ALSO REMOVING CONTAINER OF APP CONTAINERS...');
		await app.update({
			containers: app.containers.filter(containerId => containerId != foundContainerInfo.Id)
		})
		console.log('REMOVED.');
		
	}
	
	const newContainer = await module.exports.createAppContainer(app)
	
	await new Promise((resolve, reject) => {
		setTimeout(() => {resolve()}, 3000)
	})
	
	if (wasRunning) {
		console.log('STARTING CONTAINER...')
		await newContainer.start()
		this.setStatus.running(app)
		console.log('CONTAINER STARTED')
	} else {
		this.setStatus.notInitialized(app)
	}
}

module.exports.stopAppContainer = async (app) => {
	console.log('STOPING APP CONTAINER...')
	const foundContainerInfo = await this.getDockerContainerOf(app)
	
	if (foundContainerInfo) {
		if (foundContainerInfo.State == 'running') {
			this.setStatus.stoppingContainer(app)
			const container = getContainerBy(foundContainerInfo.Id)
			await container.stop()
			console.log('CONTAINER STOPED.')
			this.setStatus.finishProcessing(app)
		}
	}
}

module.exports.appStatus = async (app) => {
	if (!app) return 'App not found.'
	// INITIALIZE STATE OF APP ON MEMORY IF ALREADY NOT SET
	if (!global.appsState[app.id]) 
		global.appsState[app.id] = { state: 'not initialized', processing: false }
	
	const foundContainer = await this.getDockerContainerOf(app)
	
	let status = global.appsState[app.id]
	
	// CONTAINER FOUND AND APP STATE IS NOT PROCESSING
	if (foundContainer && global.appsState[app.id].processing == false) {
		// console.log('CHANGE STATUS TO CONTAINER STATE')
		status.state = foundContainer.State
		if (foundContainer.State == 'running') {
			status.processing = false
		}
	}
	
	
	return status
}

module.exports.isImageAvailable = async (app) => {
	const dockerImages = await listImages()
	return dockerImages && dockerImages.some(img => img.RepoTags && img.RepoTags.some(repo => repo == app.image + ':' + app.tag))
}

module.exports.pullImageOf = async (app) => {
	
	await new Promise((resolve, reject) => {
		console.log('START PULLING IMAGE...')
		pullImage(app.image, app.tag, (err, stream) => {
			if (err) console.log('ERROR:', err)
			if (err) return res.status(err.statusCode || 500).send(err)
			
			console.log("[ STARTING PULL OF " + app.image + ':' + app.tag + " FROM DOCKERHUB.com ...")
			
			stream.on('data', function(data) {
				this.setStatus.pullingImage(app)
				try {
					console.log(JSON.parse(data.toString().trim()).status)
				} catch (error) {
					console.log(data.toString().trim())
				}
			});
			
			stream.on('end', function() {
				console.log("PULL OF "+ app.image + ':' + app.tag + " FINISHED. ]")
				resolve()
			});
		})
	})
	
}