const path = require('path')
const dcManager = require('docker-composer-manager');
const dockerfileService = require('../files/dockerfile')
const { spawn } = require('child_process');
const exec = require('../../util/exec')

const buildLogs = []

module.exports.createDockerizyNetwork = async () => {
	return new Promise((resolve, reject) => {

		const out = spawn('docker', ['network', 'create', '-d', 'bridge', 'dockerizy']);
		
		let logs = ''
		let err = ''
		
		out.stdout.on('data', (data) => {
			logs += data
		});
		
		out.stderr.on('data', (data) => {
			err += data
		});
		
		out.on('close', async (code) => {
			if (!code) {
				console.log('DOCKERIZY NETWORK CREATED')
				resolve(logs)
			} else {
				if (!err.includes('dockerizy already exists'))
					console.log('CREATE DOCKERIZY NETWORK ERROR: ', err)
				resolve(err)
			}
		});
	})
}

module.exports.up = async (app) => {
	const file = path.normalize(__dirname + '../../../../apps/' + app.name + '/dockerizy/docker-compose.yml');

	return new Promise((resolve, reject) => {
		dcManager.dockerComposeUp(file).then(out => {
			// dcManager.dockerComposeUp(file, '--rm').then(out => {
			resolve(out);
		}, err => {
			reject(err);
		});
	})
}

module.exports.start = async (app) => {
	const file = path.normalize(__dirname + '../../../../apps/' + app.name + '/dockerizy/docker-compose.yml');

	return new Promise((resolve, reject) => {
		dcManager.dockerComposeStart(file).then(out => {
			// dcManager.dockerComposeUp(file, '--rm').then(out => {
			resolve(out);
		}, err => {
			this.up(app)
				.catch(err => {
					reject(err)
				})
				.then(result => {
					resolve(result)
				})
		});
	})
}

module.exports.down = async (app) => {
	const file = path.normalize(__dirname + '../../../../apps/' + app.name + '/dockerizy/docker-compose.yml');
	return new Promise((resolve, reject) => {
		dcManager.dockerComposeDown(file, '-v').then(out => {
			console.log('DOWN =>', app.name, '- removed all containers and volumes')
			resolve(out);
		}, err => {
			reject(err);
		});
	})
}

module.exports.downApp = async (app) => {
	const file = path.normalize(__dirname + '../../../../apps/' + app.name + '/dockerizy/docker-compose.yml');
	return new Promise((resolve, reject) => {
		dcManager.dockerComposeDown(file, ['--volumes', '--rmi', 'all']).then(out => {
			console.log('DOWN =>', app.name, '- removed all containers, images and volumes')
			resolve(out);
		}, err => {
			reject(err);
		});
	})
}

module.exports.stop = async (app) => {
	const file = path.normalize(__dirname + '../../../../apps/' + app.name + '/dockerizy/docker-compose.yml');
	return new Promise((resolve, reject) => {
		dcManager.dockerComposeStop(file).then(out => {
			console.log('STOP =>', app.name)
			resolve(out);
		}, err => {
			reject(err);
		});
	})
}

module.exports.refresh = async (app) => {
	return new Promise((resolve, reject) => {

		console.log('DOCKER COMPOSE BUILD ' + app.name + '...')
		const file = path.normalize(__dirname + '../../../../apps/' + app.name + '/dockerizy/docker-compose.yml');
		const out = spawn('docker-compose', ['-f', file, 'build']);

		let logs = ''
		let err = ''

		buildLogs[app.name] = ''

		out.stdout.on('data', (data) => {
			logs += data
			buildLogs[app.name] += data
		});

		out.stderr.on('data', (data) => {
			err += data
			buildLogs[app.name] += data
		});

		out.on('close', async (code) => {
			if (!code) {
				console.log('docker-compose BUILD' + app.name + ' >>> finish')
				await this.up(app)
				resolve(logs)
			} else {
				console.log('docker-compose BUILD' + app.name + ' >>> ERROR', err)
				reject(err)
			}
		});
	})
}

module.exports.getStatus = async (appName) => {
	return new Promise((resolve, reject) => {
		const container = 'dockerizy_' + appName
		const out = spawn('docker', ['ps', '-a', '--filter', 'name=' + container]);

		let status = ''
		let err = ''

		out.stdout.on('data', (data) => {
			status += data
		});

		out.stderr.on('data', (data) => {
			err += data
		});

		out.on('close', (code) => {
			if (!code) {
				try {
					const startIndex = status.indexOf('STATUS')
					const endIndex = status.indexOf('PORTS')
					status = status.match(/[^\r\n]+/g);
					status = status[1].substring(startIndex, endIndex).trim()
				} catch (error) {
					// console.log('Error format PORTS string', error)    
					return resolve('not running')
				}
				resolve(status)
			} else {
				console.log('Error get STATUS', err)
				reject(err)
			}
		});
	})
}

module.exports.getCmd = async (app) => {
	return new Promise((resolve, reject) => {
		const out = spawn('docker' , ['inspect', '-f', '{{.Config.Cmd}}', 'dockerizy_' + app.name])

		let cmd = ''
		let err = ''

		out.stdout.on('data', (data) => {
			cmd += data
		});

		out.stderr.on('data', (data) => {
			err += data
		});

		out.on('close', (code) => {
			if (!code) {
				const workingDir = spawn('docker' , ['inspect', '-f', '{{.Config.WorkingDir}}', 'dockerizy_' + app.name])

				let dir = ''
				let err = ''

				workingDir.stdout.on('data', (data) => {
					dir += data
				});

				workingDir.stderr.on('data', (data) => {
					err += data
				});

				workingDir.on('close', (code) => {
					if (!code) {
						resolve(dir ? dir + ' ' + cmd : cmd)
					} else {
						console.log('Error get WorkingDir', err)
						reject(err)
					}
				});
			} else {
				// console.log('Error get WorkingDir', err)
				reject(err)
			}
		});
	})
}

module.exports.getPorts = async (appName) => {
	return new Promise((resolve, reject) => {
		const container = 'dockerizy_' + appName
		const out = spawn('docker', ['inspect', '--format', "'{{.NetworkSettings.Ports}}'", container]);

		let ports = ''
		let err = ''

		out.stdout.on('data', (data) => {
			ports += data
		});

		out.stderr.on('data', (data) => {
			err += data
		});

		out.on('close', (code) => {
			if (!code) {

				if (ports.includes('map[]')) return resolve([])

				try {
					ports = ports.toString('utf-8').replace(/(?:\r\n|\r|\n)/g, '').replace('map[', '')
					ports = ports.substring(0, ports.length - 2).replace(new RegExp('\'', 'g'), '')
					ports = ports.replace(new RegExp('{', 'g'), '').replace(new RegExp('}', 'g'), '')
					ports = ports.replace(new RegExp('] ', 'g'), ']] ').split('] ')
					ports = ports.map(port => {
						return 'container:' + port.replace('/tcp', '').replace(':[0.0.0.0 ', ' host:').replace(':[]', ' host:not-binded').replace(']', '')
					})
					ports = ports.map(port => ({
						container: port.split(' ')[0].split(':')[1],
						host: port.split(' ')[1].split(':')[1]
					}))
				} catch (error) {
					// console.log('Error format PORTS string', error)    
				}
				resolve(ports)
			} else {
				// console.log('Error get PORTS', err)
				reject(err)
			}
		});
	})
}

module.exports.getPortsOfContainer = async (container) => {
	return new Promise((resolve, reject) => {
		const out = spawn('docker', ['inspect', '--format', "'{{.NetworkSettings.Ports}}'", container]);

		let ports = ''
		let err = ''

		out.stdout.on('data', (data) => {
			ports += data
		});

		out.stderr.on('data', (data) => {
			err += data
		});

		out.on('close', (code) => {
			if (!code) {

				if (ports.includes('map[]')) return resolve([])

				try {
					ports = ports.toString('utf-8').replace(/(?:\r\n|\r|\n)/g, '').replace('map[', '')
					ports = ports.substring(0, ports.length - 2).replace(new RegExp('\'', 'g'), '')
					ports = ports.replace(new RegExp('{', 'g'), '').replace(new RegExp('}', 'g'), '')
					ports = ports.replace(new RegExp('] ', 'g'), ']] ').split('] ')
					ports = ports.map(port => {
						return 'container:' + port.replace('/tcp', '').replace(':[0.0.0.0 ', ' host:').replace(':[]', ' host:not-binded').replace(']', '')
					})
					ports = ports.map(port => ({
						container: port.split(' ')[0].split(':')[1],
						host: port.split(' ')[1].split(':')[1]
					}))
				} catch (error) {
					// console.log('Error format PORTS string', error)    
				}
				resolve(ports)
			} else {
				// console.log('Error get PORTS', err)
				reject(err)
			}
		});
	})
}

module.exports.codeServerPath = async (appName, repoName, repoId) => {
	return new Promise((resolve, reject) => {
		exec.pwd()
			.then(async pwd => {
				console.log('RUN CODE-SERVER for ', repoName, ':', repoId, 'with password:', repoName)
				const out = spawn('docker', ['run', '--name', `dockerizy_code-server_repo-${repoId}`, '-d', '--rm', '-p',  '8080',
					'--network', 'dockerizy',
					'-v', `${pwd}/code-server/:/home/coder/.local/share/code-server`,
					'-v',  `${pwd}/apps/${appName}/dockerizy/repositories/${repoName}:/home/coder/project`,
					'-e', 'PASSWORD=' + repoName,
					// '-u', '0',
					// 'code-server-local:latest',
					// '--entrypoint', 'dumb-init',
					'codercom/code-server:v2',
					// 'code-server',
					// '--no-auth',
					// '--host', '0.0.0.0'
				]);
		
				let logs = ''
				let err = ''
		
				out.stdout.on('data', (data) => {
					logs += data
				});
		
				out.stderr.on('data', (data) => {
					err += data
				});
		
				out.on('close', async (code) => {
					if (!code) {
						console.log('run code-server >>> finish')
						resolve(logs)
					} else {
						reject(err)
					}
				});
			})
			.catch(err => {
				reject(err)
			})
	})
}

module.exports.getBuildLogs = (appName) => {
	return buildLogs[appName]
}

module.exports.getLogs = async (appName) => {
	return new Promise((resolve, reject) => {
		// const container = 'dockerizy_' + appName
		const file = path.normalize(__dirname + '../../../../apps/' + appName + '/dockerizy/docker-compose.yml');
		const out = spawn('docker-compose', ['-f', file, 'logs', '-t']);

		let logs = ''
		let err = ''

		out.stdout.on('data', (data) => {
			logs += data
		});

		out.stderr.on('data', (data) => {
			err += data
		});

		out.on('close', (code) => {
			if (!code) {
				resolve(logs)
			} else {
				console.log('Error get LOGS', err)
				reject(err)
			}
		});
	})
}

module.exports.getIp = async (appName) => {
	return new Promise((resolve, reject) => {
		const container = 'dockerizy_' + appName;
		dcManager.dockerInspectIPAddressOfContainer(container, {
			network: "bridge"
		}).then(ip => {
			resolve(ip);
		}, err => {
			reject(err);
		});
	})

}