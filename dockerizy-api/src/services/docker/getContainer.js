const Docker = require('@docker')

module.exports = (id) => {
	return Docker.getContainer(id)
}