const Docker = require('@docker')

module.exports = (id, options, result) => {
	const container = Docker.getContainer(id)
	container.update(options,function (err, data) {
		if (err) return result(err)
		return result(null, data)
	  });
}