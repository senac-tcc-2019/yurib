const Docker = require('@docker')

module.exports = (id, result) => {
	const container = Docker.getContainer(id)
	container.start(function (err, data) {
		if (err) return result(err)
		return result(null, data)
	  });
}