const Dockerode = require('dockerode');

let docker

console.error('< < < CONNECTING WIH DOCKER . . . > > >')
if (process.platform === 'win32') {
	console.error('< < < Windows > localhost:2375 > > >')
	docker = new Dockerode({
		host: 'localhost',
		port: 2375,
		socketPath: false 
	})
} else {
	const socketPath = process.env.SOCKET_PATH ? process.env.SOCKET_PATH : '/var/run/docker.sock'
	console.error('< < < Linux > Socket Path: ' + socketPath + ' > > >')
	docker = new Dockerode({ socketPath });
}
async function testConnection() {

	try {
		await docker.listContainers()
		console.error('< < < DOCKER CONNECTION SUCCESSFUL > > >')
	} catch (error) {
		console.error('< < < DOCKER CONNECT FAILED: ' + error + ' > > >')
		if (process.platform === 'win32') 
			console.error('< < < MAKE SURE DOCKER IS CONFIGURED TO ALLOW CONNECTION ON PORT 2375 OVER TCP > > >') 
	}
}

testConnection()

module.exports = docker