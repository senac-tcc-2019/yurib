const Docker = require('@docker')

module.exports = (image, tag, name, Run, Env) => {
	return Docker.createContainer({
		// Image: 'node:8-alpine',
		Image: image + ':' + tag,
		// name: 'dockerizy-container',
		name: 'dockerizy_' + name + '.' + tag,
		//Env: [
		// 'PORT=8080'
		// ],
		Env: Env.map(el => el.key + "=" + el.value),
		// Run: ['/bin/bash'],
		Run,
		// Run: ["/bin/sh -c apt-get update"],
		// ExposedPorts: {
		// 	"80/tcp": {}
		// },
		// HostConfig: {
		// 	// PortBindings: { "8888/tcp": [{ HostPort: "" }] } // automatic host port
		// 	PortBindings: { "80/tcp": [{ HostPort: "3999/tcp" }] } // 
		// },
		Tty: true,
		OpenStdin: false,
		StdinOnce: false,
		AttachStdin: false,
		AttachStdout: true,
		AttachStderr: true,
	})
}