const Docker = require('@docker')

module.exports = () => {
	return Docker.listContainers({ all: true })
}