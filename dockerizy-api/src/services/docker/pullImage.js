const Docker = require('@docker')

module.exports = (image, tag, cb) => {
	Docker.pull(image + ":" + tag, function (err, stream) {
		if (err) return cb(err)
		cb(null, stream)
	});
}