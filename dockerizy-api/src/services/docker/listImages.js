const Docker = require('@docker')

module.exports = () => {
	return Docker.listImages()
}