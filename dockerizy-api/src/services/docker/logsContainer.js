const Docker = require('@docker')

module.exports = (id) => {
	const container = Docker.getContainer(id)
	return container.logs({ id,  stdout: true, timestamps: true })
}