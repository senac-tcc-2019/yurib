'use strict';

module.exports = {
	up: (queryInterface, Sequelize) => {
		console.log('creating start favorites...')
		return queryInterface.bulkInsert('Images', [
			{
			  "name": "node",
			  "slug": "node",
			  "description": "Node.js is a JavaScript-based platform for server-side and networking applications.",
			  "logoUrl": "https://d1q6f0aelx0por.cloudfront.net/product-logos/a8957077-bbcd-41cc-b6d0-2924a1492e99-node.png",
			  "popularity": 1187510043,
			  "favorite": true,
			},
			{
			  "name": "python",
			  "slug": "python",
			  "description": "Python is an interpreted, interactive, object-oriented, open-source programming language.",
			  "logoUrl": "https://d1q6f0aelx0por.cloudfront.net/product-logos/6bd224a8-e827-4593-b5b4-483338e9999e-python.png",
			  "popularity": 534891897,
			  "favorite": true,
			},
			{
			  "name": "openjdk",
			  "slug": "openjdk",
			  "description": "OpenJDK is an open-source implementation of the Java Platform, Standard Edition",
			  "logoUrl": "https://d1q6f0aelx0por.cloudfront.net/openjdk2.png",
			  "popularity": 493375900,
			  "favorite": true,
			},
			{
			  "name": "golang",
			  "slug": "golang",
			  "description": "Go (golang) is a general purpose, higher-level, imperative programming language.",
			  "logoUrl": "https://d1q6f0aelx0por.cloudfront.net/product-logos/81630ec2-d253-4eb2-b36c-eb54072cb8d6-golang.png",
			  "popularity": 658948604,
			  "favorite": true,
			},
			{
			  "name": "nginx",
			  "slug": "nginx",
			  "description": "Official build of Nginx.",
			  "logoUrl": "https://d1q6f0aelx0por.cloudfront.net/fa443219-42e0-4886-96b4-8733de694b72-c641a5d6-1ebf-44ee-9607-aef9b4ca1a3b-logo_large.png",
			  "popularity": 1639855427,
			  "favorite": true,
			},
			{
			  "name": "httpd",
			  "slug": "httpd",
			  "description": "The Apache HTTP Server Project",
			  "logoUrl": "https://d1q6f0aelx0por.cloudfront.net/product-logos/b078c572-d43d-46e0-9fa2-8e2d31cb5ce2-httpd.png",
			  "popularity": 1306534095,
			  "favorite": true,
			},
			{
			  "name": "wordpress",
			  "slug": "wordpress",
			  "description": "The WordPress rich content management system can utilize plugins, widgets, and themes.",
			  "logoUrl": "https://d1q6f0aelx0por.cloudfront.net/product-logos/fcbd05b8-ec7e-4191-80f6-dae8ec3d9d25-wordpress.png",
			  "popularity": 377764729,
			  "favorite": true,
			},
			{
			  "name": "ubuntu",
			  "slug": "ubuntu",
			  "description": "Ubuntu is a Debian-based Linux operating system based on free software.",
			  "logoUrl": "https://d1q6f0aelx0por.cloudfront.net/product-logos/2ee79de2-ebfe-4b84-8eae-3c302a275b88-ubuntu.png",
			  "popularity": 1439607908,
			  "favorite": true,
			},
			{
			  "name": "centos",
			  "slug": "centos",
			  "description": "The official build of CentOS.",
			  "logoUrl": "https://d1q6f0aelx0por.cloudfront.net/product-logos/4fae2c05-6d9d-422a-a317-f501cd0ffd3c-centos.png",
			  "popularity": 561345342,
			  "favorite": true,
			},
			{
			  "name": "debian",
			  "slug": "debian",
			  "description": "Debian is a Linux distribution that's composed entirely of free and open-source software.",
			  "logoUrl": "https://d1q6f0aelx0por.cloudfront.net/product-logos/4a4d002e-a9a7-4bbe-b51d-9d780daf8f41-debian.png",
			  "popularity": 283758871,
			  "favorite": true,
			},
			{
			  "name": "alpine",
			  "slug": "alpine",
			  "description": "A minimal Docker image based on Alpine Linux with a complete package index and only 5 MB in size!",
			  "logoUrl": "https://d1q6f0aelx0por.cloudfront.net/product-logos/2ebdc784-9c22-4b27-adb7-b56079682762-alpine.png",
			  "popularity": 1662135403,
			  "favorite": true,
			},
			{
			  "name": "mongo",
			  "slug": "mongo",
			  "description": "MongoDB document databases provide high availability and easy scalability.",
			  "logoUrl": "https://www.computing.co.uk/w-images/10398b94-833a-4596-a955-f4e351b1998f/0/mongodblogo-580x358.png",
			  "popularity": 1152337088,
			  "favorite": true,
			},
			{
			  "name": "mysql",
			  "slug": "mysql",
			  "description": "MySQL is a widely used, open-source relational database management system (RDBMS).",
			  "logoUrl": "https://d1q6f0aelx0por.cloudfront.net/product-logos/0dd7193f-e747-4a15-b797-818b9fac3656-mysql.png",
			  "popularity": 1136957395,
			  "favorite": true,
			},
			{
			  "name": "mariadb",
			  "slug": "mariadb",
			  "description": "MariaDB is a community-developed fork of MySQL intended to remain free under the GNU GPL.",
			  "logoUrl": "https://d1q6f0aelx0por.cloudfront.net/product-logos/d8cef0f8-8a8f-4d8f-8efb-e6d40560fc82-mariadb.png",
			  "popularity": 845802990,
			  "favorite": true,
			},
			{
			  "name": "postgres",
			  "slug": "postgres",
			  "description": "The PostgreSQL object-relational database system provides reliability and data integrity.",
			  "logoUrl": "https://d1q6f0aelx0por.cloudfront.net/product-logos/a28dcd12-094d-4248-bfcc-f6fb954c7ab8-postgres.png",
			  "popularity": 1516224091,
			  "favorite": true,
			},
			{
			  "name": "redis",
			  "slug": "redis",
			  "description": "Redis is an open source key-value store that functions as a data structure server.",
			  "logoUrl": "https://d1q6f0aelx0por.cloudfront.net/product-logos/89e5782a-76ea-4b94-a561-39e331c281a5-redis.png",
			  "popularity": 1153508832,
			  "favorite": true,
			},
			{
			  "name": "couchbase",
			  "slug": "couchbase",
			  "description": "Couchbase Server is a NoSQL document database with a distributed architecture.",
			  "logoUrl": "https://d1q6f0aelx0por.cloudfront.net/product-logos/0d32108f-9feb-4fd8-9af3-5ae6baff0090-67b8fe05-d2d0-4e4d-8224-22cfd7ae4f73-baf18caf-b63c-427f-970c-5c5aac7b2f99-logo-large.png",
			  "popularity": 55493894,
			  "favorite": true,
			}
		  ], {});
	},

	down: (queryInterface, Sequelize) => {
		/*
      Add reverting commands here.
      Return a promise to correctly handle asynchronicity.

      Example:
      return queryInterface.bulkDelete('People', null, {});
    */
	}
};
