module.exports = (sequelize, DataTypes) => {
	const Run = sequelize.define('Run', {
		id: {
			type: DataTypes.UUID,
			defaultValue: DataTypes.UUIDV4,
		},
		AppId: {
			type: DataTypes.INTEGER,
			primaryKey: true,
		},
		order: {
			type: DataTypes.INTEGER,
			allowNull: false,
			primaryKey: true,
		},
		run: {
			type: DataTypes.STRING,
			allowNull: false,
		},
		dir: {
			type: DataTypes.STRING,
			allowNull: false,
		}
	}, {
		indexes: [
			{ fields:['order'] },
		],
		timestamps: false,
	});

	Run.associate = function (models) {
		Run.belongsTo(models.App)
	}

	return Run;
}