module.exports = (sequelize, DataTypes) => {
	const Env = sequelize.define('Env', {
		id: {
			type: DataTypes.UUID,
			defaultValue: DataTypes.UUIDV4
		},
		AppId: {
			type: DataTypes.INTEGER,
			primaryKey: true,
		},
		key: {
			type: DataTypes.STRING,
			allowNull: false,
			primaryKey: true,
			set(val) {
				this.setDataValue('key', val.replace(/ /g, '_'));
			}
		},
		value: {
			type: DataTypes.STRING,
			allowNull: false,
			set(val) {
				this.setDataValue('value', val.replace(/ /g, '_'));
			}
		},
	}, {
		indexes: [
			{ fields:['key'] },
		],
		timestamps: false,
	});

	Env.associate = function (models) {
		Env.belongsTo(models.App)
	}

	return Env;
}