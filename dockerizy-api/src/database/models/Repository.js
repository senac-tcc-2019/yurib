module.exports = (sequelize, DataTypes) => {
	const Repository = sequelize.define('Repository', {
		id: {
			type: DataTypes.INTEGER,
			autoIncrement: true,
			primaryKey: true
		},
		name: {
			type: DataTypes.STRING,
			allowNull: false,
			set(val) {
				this.setDataValue('name', val.replace(/ /g, '-'));
			}
		},
		url: {
			type: DataTypes.STRING,
			allowNull: false
		},
		branch: {
			type: DataTypes.STRING,
			allowNull: false
		},
		user: {
			type: DataTypes.STRING,
			allowNull: false
		},
		password: {
			type: DataTypes.STRING,
			allowNull: false
		},
		target: {
			type: DataTypes.STRING,
			allowNull: false
		},
	}, 
	{ 
		timestamps: false 
	});

	Repository.associate = function (models) {
		Repository.belongsTo(models.App)
	}

	return Repository;
}