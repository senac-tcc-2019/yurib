module.exports = (sequelize, DataTypes) => {
	
	const separator = ' -/- '

	const App = sequelize.define('App', {
		id: {
			type: DataTypes.INTEGER,
			autoIncrement: true,
			primaryKey: true
		},
		name: {
			type: DataTypes.STRING,
			allowNull: false,
			set(val) {
				this.setDataValue('name', val.replace(/ /g, '-'));
			}
		},
		image: {
			type: DataTypes.STRING,
			allowNull: false
		},
		tag: {
			type: DataTypes.STRING,
			allowNull: false
		},
		logoUrl: {
			type: DataTypes.STRING
		},
		containers: {
			type: DataTypes.TEXT,
			get() {
				try {
					return this.getDataValue('containers').split(separator)
				} catch (error) {
					return []
				}
			},
			set(val) {
				this.setDataValue('containers', val.join(separator).length > 0 ? val.join(separator) : null);
			},
		},
		cmd: {
			type: DataTypes.STRING,
		}
	}, {
		indexes: [
			{ unique:true, fields:['name'] },
		],
		timestamps: false
	});

	App.associate = function (models) {
		App.hasMany(models.Service)
		App.hasMany(models.Repository)
		App.hasMany(models.Env, {
			targetKey: 'key'
		})
		App.hasMany(models.Port, {
			targetKey: 'name'
		})
		App.hasMany(models.Run, {
			targetKey: 'order'
		})
	}

	return App;
}