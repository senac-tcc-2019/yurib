module.exports = (sequelize, DataTypes) => {
	const Image = sequelize.define('Image', {
		id: {
			type: DataTypes.INTEGER,
			autoIncrement: true,
			primaryKey: true 
		},
		name: {
			type: DataTypes.STRING,
			allowNull: false
		},
		slug: {
			type: DataTypes.STRING,
			allowNull: false,
		},
		description: {
			type: DataTypes.STRING
		},
		logoUrl: {
			type: DataTypes.STRING
		},
		popularity: {
			type: DataTypes.BIGINT,
			defaultValue: 0
		},
		favorite: {
			type: DataTypes.BOOLEAN
		},
	}, 
	{ timestamps: false });

	return Image;
}