module.exports = (sequelize, DataTypes) => {
	const Port = sequelize.define('Port', {
		id: {
			type: DataTypes.UUID,
			defaultValue: DataTypes.UUIDV4
		},
		AppId: {
			type: DataTypes.INTEGER,
			primaryKey: true,
		},
		name: {
			type: DataTypes.STRING,
			allowNull: false,
			primaryKey: true,
		},
		port: {
			type: DataTypes.STRING,
			allowNull: false,
		},
	}, {
		indexes: [
			{ fields:['name'] },
		],
		timestamps: false,
	});

	Port.associate = function (models) {
		Port.belongsTo(models.App)
	}

	return Port;
}