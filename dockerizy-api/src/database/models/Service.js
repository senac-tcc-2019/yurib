module.exports = (sequelize, DataTypes) => {
	const Service = sequelize.define('Service', {
		id: {
			type: DataTypes.INTEGER,
			autoIncrement: true,
			primaryKey: true
		},
		name: {
			type: DataTypes.STRING,
			allowNull: false,
			set(val) {
				this.setDataValue('name', val.replace(/ /g, '-'));
			}
		},
		image: {
			type: DataTypes.STRING,
			allowNull: false
		},
		tag: {
			type: DataTypes.STRING,
			allowNull: false
		},
		logoUrl: {
			type: DataTypes.STRING
		},
		containers: {
			type: DataTypes.TEXT,
			get() {
				try {
					return this.getDataValue('containers').split(separator)
				} catch (error) {
					return []
				}
			},
			set(val) {
				this.setDataValue('containers', val.join(separator));
			},
		},
		volumes: {
			type: DataTypes.TEXT,
			get() {
				try {
					return this.getDataValue('volumes').split(separator)
				} catch (error) {
					return []
				}
			},
			set(val) {
				this.setDataValue('volumes', val.join(separator));
			},
		},
		runs: {
			type: DataTypes.TEXT,
			get() {
				try {
					return this.getDataValue('runs').split(separator)
				} catch (error) {
					return []
				}
			},
			set(val) {
				this.setDataValue('runs', val.join(separator).length > 0 ? val.join(separator) : null);
			},
		},
		run: {
			type: DataTypes.STRING,
		}
	}, 
	{ 
		timestamps: false 
	});

	Service.associate = function (models) {
		Service.belongsTo(models.App)
	}

	return Service;
}