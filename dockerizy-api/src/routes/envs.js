const EnvController = require('@controller/envs');

module.exports = function(app) {
	
	app.post('/api/env', (req, res) => {
		EnvController.newEnv(req, res);
	});

	app.delete('/api/env/:id', (req, res) => {
		EnvController.deleteEnv(req, res);
	});
	
};


