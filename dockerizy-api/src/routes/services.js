const ServiceController = require('@controller/services');

module.exports = function(app) {
	
	app.post('/api/services', (req, res) => {
		ServiceController.newService(req, res);
	});

	app.get('/api/services', (req, res) => {
		ServiceController.getServices(req, res);
	});

	app.get('/api/services/:id', (req, res) => {
		ServiceController.getService(req, res);
	});

	app.put('/api/services/:id', (req, res) => {
		ServiceController.putService(req, res);
	});

	app.delete('/api/services/:id', (req, res) => {
		ServiceController.deleteService(req, res);
	});

};


