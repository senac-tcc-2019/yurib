const PortController = require('@controller/ports');

module.exports = function(app) {
	
	app.post('/api/port', (req, res) => {
		PortController.newPort(req, res);
	});

	app.delete('/api/port/:id', (req, res) => {
		PortController.deletePort(req, res);
	});
	
};


