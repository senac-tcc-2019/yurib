const ImagesController = require('@controller/images');

module.exports = function(app) {
	
	app.post('/api/images/pull', (req, res) => {
		ImagesController.pull(req, res);
	});

	app.get('/api/images/list', (req, res) => {
		ImagesController.list(req, res);
	});

	app.get('/api/images/favorites', (req, res) => {
		ImagesController.favorites(req, res);
	});

	app.post('/api/images/favorites', (req, res) => {
		ImagesController.newFavorite(req, res);
	});
	
};


