const ContainersController = require('@controller/containers');

module.exports = function(app) {
	
	app.post('/api/containers/create', (req, res) => {
		ContainersController.create(req, res);
	});

	app.get('/api/containers/list', (req, res) => {
		ContainersController.list(req, res);
	});

};


