const AppController = require('@controller/apps');

module.exports = function(app) {
	
	const route = '/api/apps'

	// INIT APP
	app.post(route + '/init/:id', (req, res) => {
		AppController.initApp(req, res);
	});

	// INIT APP
	app.post(route + '/start/:id', (req, res) => {
		AppController.startApp(req, res);
	});

	// STOP APP (COMPOSE DOWN)
	app.post(route + '/stop/:id', (req, res) => {
		AppController.stopApp(req, res);
	});

	// REFRESH APP (RECREATE CONTAINER)
	app.post(route + '/refresh/:id', (req, res) => {
		AppController.refreshApp(req, res);
	});

	// CLEAR APP (REMOVE CONTAINER/VOLUMES)
	app.post(route + '/clear/:id', (req, res) => {
		AppController.clearApp(req, res);
	});

	// CREATE APP
	app.post(route, (req, res) => {
		AppController.newApp(req, res);
	});

	// GET LOGS OF APP BY ID
	app.get(route + '/logs/:id', (req, res) => {
		AppController.logs(req, res);
	});

	// GET BUILD LOGS OF APP BY ID
	app.get(route + '/build-logs/:id', (req, res) => {
		AppController.buildLogs(req, res);
	});

	// GET CMD OF APP BY ID
	app.get(route + '/cmd/:id', (req, res) => {
		AppController.getCmd(req, res);
	});

	// LIST ALL APPS
	app.get(route, (req, res) => {
		AppController.getApps(req, res);
	});

	// GET APP BY ID
	app.get(route + '/:id', (req, res) => {
		AppController.getApp(req, res);
	});

	// GET APP BY NAME
	app.get(route + '/name/:name', (req, res) => {
		AppController.getAppByName(req, res);
	});

	// UPDATE APP BY ID
	app.put(route + '/:id', (req, res) => {
		AppController.putApp(req, res);
	});

	// DELETE APP BY ID
	app.delete(route + '/:id', (req, res) => {
		AppController.deleteApp(req, res);
	});

};


