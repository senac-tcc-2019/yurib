const dockerHub = require('@util/dockerHub');
const favorites = require('@model/Image');

module.exports = function(app) {

	app.post('/api/hub/search', async (req, res) => {
		const result = await dockerHub.searchImage(req.body)
		res.send(result)
	})

	app.post('/api/hub/search/tags', async (req, res) => {
		const result = await dockerHub.getTagsFrom(req.body)
		res.send(result)
	})

};
