const RunController = require('@controller/runs');

module.exports = function(app) {
	
	app.post('/api/run', (req, res) => {
		RunController.newRun(req, res);
	});

	app.delete('/api/run/:id', (req, res) => {
		RunController.deleteRun(req, res);
	});
	
};


