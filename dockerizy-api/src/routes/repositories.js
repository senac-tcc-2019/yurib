const RepositoriesController = require('@controller/repositories');

module.exports = function(app) {
	
	app.post('/api/repository', (req, res) => {
		RepositoriesController.newRepository(req, res);
	});

	app.delete('/api/repository/:id', (req, res) => {
		RepositoriesController.deleteRepository(req, res);
	});
};


