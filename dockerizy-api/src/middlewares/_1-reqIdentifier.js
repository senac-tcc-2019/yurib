

var makeID = (function() {
	var index = 0;
	return function() {
	 return index++;
	}
})();
  

module.exports = function(app) {

	app.use(function(req, res, next) {
		req.id = makeID()
		next()
	  })

};