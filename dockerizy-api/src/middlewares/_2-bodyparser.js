const bodyparser = require('body-parser');

module.exports = function(app) {

	// parse application/x-www-form-urlencoded
	app.use(bodyparser.urlencoded({ extended: false }));
	// parse application/json
	app.use(bodyparser.json());

};