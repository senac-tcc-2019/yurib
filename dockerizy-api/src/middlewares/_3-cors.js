module.exports = function(app) {
	app.use(function(req, res, next) {

		res.header(
			'Access-Control-Allow-Methods',
			'OPTIONS, GET, POST, PUT, DELETE'
		);
		res.header(
			'Access-Control-Allow-Headers',
			'Origin, X-Requested-With, Content-Type, Accept, Authorization'
		);

		res.header('Access-Control-Allow-Origin', '*');
		
		if ('OPTIONS' === req.method) {
			res.status(200).end();
		} else {
			next();
		}
	});
};
