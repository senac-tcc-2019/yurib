const { App: AppModel } = require('@model');
const compose = require('../services/docker-compose/compose')
const hosts = require('@util/hosts')
const RepositoriesController = require('@controller/repositories');

module.exports = function(app) {
	const dotDockerizyDomain = '.' + hosts.localDomain
	const codeRepositoryDomain = '.code' + dotDockerizyDomain

	var makeID = (function() {
		var index = 0;
		return function() {
		 return index++;
		}
	})();

	const colors = [
		// "\x1b[30m", //FgBlack: 
		"\x1b[31m", //FgRed: 
		"\x1b[32m", //FgGreen: 
		"\x1b[33m", //FgYellow: 
		"\x1b[34m", //FgBlue: 
		"\x1b[35m", //FgMagenta: 
		"\x1b[36m", //FgCyan: 
		// "\x1b[37m", //FgWhite: 
	]

	appsRepositoryResolver = async function(host, url, req) {
		const reqId = makeID()
		if(host.includes(codeRepositoryDomain)) {
			try {
				console.log('>>'+reqId+'REQUESTING CODE-SERVER...'.green)
				const subDomain = host.split(codeRepositoryDomain)[0]
				const appName = subDomain.split('.')[0]
				const repoName = subDomain.split('.')[1]
				const repositoryUrl = await RepositoriesController.getCodeRepository(appName, repoName);
				console.log('<<'+reqId+'PROXYING CLIENT TO CODE-SERVER'.blue, repositoryUrl)
				// console.log('\x1b[0m')
				return repositoryUrl
			} catch (error) {
				console.log('FAIL TO PROXY CODE-SERVER'.green)
			}
		}
	};

	appsServiceResolver = async function(host, url, req) {
		// console.log(colors[Math.round(Math.random() * (colors.length -1))]);

		const reqId = makeID()
		if(host && host.includes(dotDockerizyDomain) && !host.includes(codeRepositoryDomain)) {

			try {
				const subDomain = host.split(dotDockerizyDomain)[0]
				const appName = subDomain.split('.')[0]
				const portName = subDomain.split('.')[1]

				const dbApp = await AppModel.findOne({
					where: {
						name: appName
					},
					include: {
						all: true,
						nested: true
					}
				})

				const containerServicePort = dbApp.Ports.find(port => port.name == portName)
				const containerHostsPorts = await compose.getPorts(appName)
				const exposedPort = containerHostsPorts.find(port => port.container == containerServicePort.port)
				console.log('trying to proxy ' + host + ' to >', exposedPort.host)

				if (exposedPort.host) {
					console.log('proxying ' + host + ' to >', exposedPort.host)
					return 'http://localhost:' + exposedPort.host;
				}
				else {
					console.error('FAIL TO PROXY', error)
				}
			} catch (error) {
				console.error('FAIL TO PROXY', error)
			}
		}
	};

	app.proxy.addResolver(appsServiceResolver)
	app.proxy.addResolver(appsRepositoryResolver)

};
