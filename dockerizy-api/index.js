process.env.HOST_DOMAIN = 'docker.izy'


// REGISTER ALLIAS FOR IMPORTS - EX: require('@routes')
require('module-alias/register') // for requires like "@models" (configured at package.json)

// colored texts by editing string prototype, ex:  console.log('my text in red'.red)
require('colors')

// ENTRY POINT AND PROXY SERVER
var proxy = require('redbird')({ 
	port: 80, 
	bunyan: {
		name: 'DOCKERIZY',
		streams: [{
			stream: process.stdout,
			level: 'fatal' // Only logs when the level is fatal or higher
		}]
	}
});
// ++++++++++++++++++++++++++++


// EXPRESS SERVER FOR API ROUTES
const express = require('express');
const app = express();

// add proxy reference at app to be used by routes and middlewares
app.proxy = proxy

// load middlewares and routes
require('@middlewares')(app);
require('@routes')(app);

// SERVE FRONT END 
app.use(express.static('../dockerizy-ui/dist'));

const PORT = process.env.PORT || 3000;

app.listen(PORT, () => {
	console.log('\n * Dockerizy running on localhost:' + PORT);
});
// +++++++++++++++++++++++++++++


// WRITE ON hosts FILE TO LOCAL SUBDOMAINS
const hosts = require('@util/hosts')

// generate etc/hosts content for local domains like http://docker.izy and apps/services like http://app-test.docker.izy, http://app-test.api.docker.izy
hosts.setDockerizyDomain()
hosts.createAppsDomain()
// +++++++++++++++++++++++++++++++++++++++

// PROXY API SERVER
proxy.register(process.env.HOST_DOMAIN, "http://localhost:" + PORT);

require('./src/services/docker-compose/compose').createDockerizyNetwork()