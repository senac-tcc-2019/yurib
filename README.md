# Dockerizy

## Plataforma web para gerenciamento de aplicações e serviços em containers

![](https://imgur.com/N9u8cdE.png)



#### Dockerizy utiliza a engine do Docker presente no servidor ou maquina host local e atua como uma PaaS(Platform as a Service) para desenvolvedores rodar e gerenciar aplicaçoes e serviços em ambientes pré-configurados.
 
#### Dashboard:
 
![](https://imgur.com/p1zCJ2Z.png)
## Features:

* Procurar imagens/ambientes pré-configurados no Dockerhub e adiciona-las aos favoritos
* Adicionar repositorios GIT que são automaticamente clonados e inseridos no ambiente
* Manipular arquivos dos repositórios diretamente no navegador com o VSCode e todas as suas extensões
* Customizar ambientes pré-configurados (build) com uma sequencia de comandos necessários para rodar uma aplicação ou serviço
* Configuração de serviço:porta - acessíveis por subdominios, ex: app.servicox.docker.izy (reverse-proxy - todas as aplicações e serviços são acessíveis pela mesma porta)
* Manipilar variáveis de ambiente
* Visualizar logs das aplicações/serviços
* Visualizar logs de build
* Camada de Serviços (area para organizar visualmente os serviços utilizados por uma aplicação, ex: banco de dados) (indisponível - em desenvolvimento. Atualmente os serviços funcionam sem essa "organização" visual)

## Instalação:

#### Requisitos: 

* Linux/MacOS - (Compatibilidade com o Windows sera implementada em versões futuras. Algumas features recentes quebraram o funcionamento)
* Docker / Docker-compose
* NodeJS
* GIT

##### Setup:

1.  Clone o projeto.
2.  cd dockerizy_ui/ (front-end)
3.  npm install (instala as dependências do front-end)
4.  npm run build (gera o html/css/js do front-end para ser servido pelo back-end)
5.  cd ../dockerizy-api (back-end)
6.  npm install (instala as dependeências do back-end)
7.  docker-compose up -d (sobe o container do banco de dados utilizado pela api na porta 3307. Caso precise mudar, deve ser alterado os arquivos index.prod.js/index.dev.js)
8.  npm run db:setup (cria o banco, tabelas e popula com alguns dados iniciais)
9.  sudo npm start (sobe a plataforma - necessita permissao root pois a api manipula o arquivo "hosts" do sistema para que funcione o dominio e subdominios local)
10. Acesso Local-> http://docker.izy (host:80)